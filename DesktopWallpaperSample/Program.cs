﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesktopWallpaper;

namespace DesktopWallpaperSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var wallpaper = (IDesktopWallpaper)(new DesktopWallpaperClass());

            for (uint i = 0; i < wallpaper.GetMonitorDevicePathCount(); i++)
            {
                var monitorId = wallpaper.GetMonitorDevicePathAt(i);
                var rect = wallpaper.GetMonitorRECT(monitorId);
                Console.WriteLine("RECT: {0} {1} {2} {3}", rect.Left, rect.Top, rect.Right, rect.Bottom);
                Console.WriteLine("Position: {0}", wallpaper.GetPosition());
                Console.WriteLine("Wallpaper: {0}", wallpaper.GetWallpaper(monitorId));
            }
            Console.ReadLine();
        }
    }
}
